'use strict';

const Ajv = require('ajv');
const co = require('co');
const fs = require('mz/fs');
const minimatch = require('minimatch');
const path = require('path');
const wait = require('co-wait');
const yaml = require('js-yaml');
const _ = require('lodash');

const schema = {
  title: "Open software base contribution schema",
  $schema: 'http://json-schema.org/draft-04/schema#',
  type: "object",
  properties: {
    name: {
      type: 'string'
    },
    description: {
      type: 'object',
      properties: {
        fr: {
          type: 'string'
        },
        en: {
          type: 'string'
        }
      },
      required: ['fr', 'en']
    }
  },
  required: ['name', 'description']
};

co(function * () {
  var files = yield fs.readdir('.');
  var yamlFiles = files
    .filter(filename => (minimatch(filename, '*.+(yaml|yml)')));

  var wrongFiles = _.difference(files, [...yamlFiles, 'README.md', '.git']);
  if (wrongFiles.length > 0) {
    console.log('New files "' + wrongFiles.join('", "') + '".');
    yield wait(1000);
    process.exit(1);
  }

  yield yamlFiles.map(co.wrap(function * (filename) {
    var ajv = new Ajv();
    var data;
    try {
      var buffer = yield fs.readFile(path.join('.', filename));
      data = yaml.load(buffer.toString());
    } catch (e) {
      console.log(filename + ' is invalid YAML', e);
      yield wait(1000);
      process.exit(1);
    }

    if (!data) {
      console.log(filename + ' is empty');
      yield wait(1000);
      process.exit(1);
    }

    var valid = ajv.validate(schema, data);

    if (!valid) {
      console.log(filename + ' is invalid');
      console.log(ajv.errors);
      yield wait(1000);
      process.exit(1);
    }

    return;
  }));

  wait(1000);
  process.exit(0);
}).catch(co.wrap(function * (err) {
  console.log(err);
  yield wait(1000);
  process.exit(1);
}));
