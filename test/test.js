/* globals it */
'use strict';

const exec = require('child_process').exec;
const path = require('path');

it('should validate valid files', function(done) {
  exec(
    'node ' + path.join(__dirname, '..', 'json_schema.js'),
    {cwd: path.join(__dirname, 'directory'),
    timeout: 0},
    function(err, stdout) {
      console.log(stdout);
      if (err) {
        return done(err);
      }

      return done();
    }
  );
});
