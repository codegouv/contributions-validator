'use strict';

const server = require('tremble-ci/server');
const path = require('path');

server({
  command: 'node ' + path.join(__dirname, 'json_schema.js'),
  dataDir: path.join(__dirname, 'data'),
  pageTitle: 'Open software base validator'
}).listen(3000, () => {
  console.log('CI server listening on port 3000 !');
});
